//
//  GameScene.m
//  Boxing
//
//  Created by 虞登翔 on 2016/1/27.
//  Copyright (c) 2016年 虞登翔. All rights reserved.
//

#import "GameViewController.h"
#import "GameScene.h"
#import "ArenaGenerator.h"
#import "PointsLabel.h"
#import "PhysicalStrength.h"
#import "Player.h"

@import AVFoundation;

const int buttonRadius = 35;
const int blueButtonPosition = -285;
const int redButtonPosition = 285;

const int blueHealthBarHeight = -205;
const int redHealthBarHeight = 185;

const int blueStrengthBarHeight = -235;
const int redStrengthBarHeight = 215;
const int sceneEnduration = 10;

const float moveDur = 0.01;
const int startCount = 3;

static NSString *GAME_FONT = @"AmericanTyperwriter-Bold";

int blueState = 0;
int redState = 0;

int adcounter = 2;

BOOL SoundEffectIsPlayed = 0;

@interface GameScene()
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property BOOL isStarted;
@property BOOL isGameOver;
@end

@implementation GameScene {
    SKLabelNode *progress;
    int currMinute;
    int currSeconds;
    NSTimer *timer;
    
    Player *player1;
    Player *player2;
}


-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        self.anchorPoint = CGPointMake(0.5, 0.5);
        
        SKLabelNode *tapToBeginLabel = [SKLabelNode labelNodeWithFontNamed:GAME_FONT];
        tapToBeginLabel.name = @"tapToBeginLabel";
        tapToBeginLabel.text = @"Tap to Fight!";
        tapToBeginLabel.fontColor = [UIColor orangeColor];
        tapToBeginLabel.fontSize = 80;
        tapToBeginLabel.position = CGPointMake(155, 10);
        tapToBeginLabel.zRotation = M_PI / 2;
        [self addChild:tapToBeginLabel];
        [self animatieWithPulse:tapToBeginLabel];
        
        [self content];
    }
    return self;
}

-(void)content {
    self.backgroundColor = [SKColor colorWithRed:0 green:0 blue:0 alpha:1.0];
    
    ArenaGenerator *generator;
    generator = [ArenaGenerator generatorWithArena];
    [self addChild:generator];
    
    player1 = [Player player1];
    player1.position = CGPointMake(0, -100);
    [self addChild:player1];
    
    player2 = [Player player2];
    player2.position = CGPointMake(0, 100);
    [self addChild:player2];
    
    [self loadBlueButton];
    [self loadRedButton];
    
    [self loadBlueScoreLabels];
    [self loadRedScoreLabels];
    
    [self loadBluePhysicalStrength];
    [self loadRedPhysicalStrength];

    NSURL *BGMusic = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/BackgroundMusic.mp3",
                                          [[NSBundle mainBundle] resourcePath]]];
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:BGMusic error:nil];
    self.audioPlayer.numberOfLoops = -1;
    self.audioPlayer.volume = 0.5;
}

-(void)start {
    self.isStarted = YES;
    [[self childNodeWithName:@"tapToBeginLabel"] removeFromParent];
    
    self.userInteractionEnabled = NO;
    
    //-- Game Time
    progress = [SKLabelNode labelNodeWithFontNamed:GAME_FONT];
    progress.fontSize = 80;
    progress.position = CGPointMake(-120, 10);
    progress.fontColor = [UIColor yellowColor];
    progress.zRotation = M_PI / 2;
    [progress setText:@"2:00"];
    
    currMinute = 2;
    currSeconds = 00;
    
    //-- 3...2...1...
    SKLabelNode *countDown = [SKLabelNode labelNodeWithFontNamed:GAME_FONT];
    countDown.fontSize = 90;
    countDown.position = CGPointMake(25, 0);
    countDown.fontColor = [UIColor yellowColor];
    countDown.zRotation = M_PI / 2;
    [self addChild: countDown];
    // Initialize the countdown variable
    __block int countDownInt = startCount;
    // Define the actions
    SKAction *updateLabel = [SKAction runBlock:^{
        countDown.text = [NSString stringWithFormat:@"%d", countDownInt];
        countDownInt--;
    }];
    SKAction *countAudio = [SKAction playSoundFileNamed:@"Countdown.mp3" waitForCompletion:NO];
    SKAction *wait = [SKAction waitForDuration:1.0];
    // Create a combined action
    SKAction *updateLabelAndWait = [SKAction sequence:@[updateLabel, countAudio, wait]];
    // Run action "seconds" number of times and then set the label to indicate
    // the countdown has ended
    [self runAction:[SKAction repeatAction:updateLabelAndWait count:startCount] completion:^{
        [self runAction:[SKAction playSoundFileNamed:@"boxingBell.wav" waitForCompletion:NO]];
        self.userInteractionEnabled = YES;
        [countDown removeFromParent];
        [self.audioPlayer play];
        [self addChild:progress];
        timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
    }];
}

-(void)timerFired {
    if((currMinute > 0 || currSeconds >= 0) && currMinute >= 0) {
        if(currSeconds == 0) {
            currMinute -=1 ;
            currSeconds = 59;
        }
        else if(currSeconds > 0) {
            currSeconds -= 1;
        }
        
        if(currMinute > -1)
            [progress setText:[NSString stringWithFormat:@"%d%@%02d",currMinute,@":",currSeconds]];
    }
    else {
        [timer invalidate];
        [self gameOver];
    }
}

- (void)loadBlueScoreLabels {
    PointsLabel *pointsLabel = [PointsLabel pointsLabelWithFontNamed:GAME_FONT];
    pointsLabel.name = @"bluePointsLabel";
    [self addChild:pointsLabel];
    
    SKShapeNode *healthBar = [SKShapeNode shapeNodeWithRect:CGRectMake(-100, blueHealthBarHeight, 200, 20)];
    healthBar.fillColor = [UIColor clearColor];
    healthBar.strokeColor = [UIColor yellowColor];
    healthBar.glowWidth = 3;
    [self addChild:healthBar];
    
    SKLabelNode *barLabel = [SKLabelNode labelNodeWithFontNamed:GAME_FONT];
    barLabel.text = @"Health";
    barLabel.position = CGPointMake(0, blueHealthBarHeight);
    barLabel.fontSize = 25;
    barLabel.fontColor = [UIColor blueColor];
    [healthBar addChild:barLabel];
    
    SKShapeNode *blueHealth = [SKShapeNode new];
    CGMutablePathRef myPath = CGPathCreateMutable();
    CGPathAddRect(myPath, NULL, CGRectMake(-100, blueHealthBarHeight, 200, 20));
    blueHealth.path = myPath;
    blueHealth.fillColor = [UIColor colorWithRed:0 green:1.0 blue:1.0 alpha:1.0];
    blueHealth.strokeColor = [UIColor clearColor];
    blueHealth.name = @"blueHealthMeter";
    [self addChild:blueHealth];
}

- (void)loadRedScoreLabels {
    PointsLabel *pointsLabel = [PointsLabel pointsLabelWithFontNamed:GAME_FONT];
    pointsLabel.name = @"redPointsLabel";
    [self addChild:pointsLabel];
    
    SKShapeNode *healthBar = [SKShapeNode shapeNodeWithRect:CGRectMake(-100, redHealthBarHeight, 200, 20)];
    healthBar.fillColor = [UIColor clearColor];
    healthBar.strokeColor = [UIColor yellowColor];
    healthBar.glowWidth = 3;
    [self addChild:healthBar];
    
    SKLabelNode *barLabel = [SKLabelNode labelNodeWithFontNamed:GAME_FONT];
    barLabel.text = @"Health";
    barLabel.position = CGPointMake(0, redHealthBarHeight + 20);
    barLabel.xScale = -1;
    barLabel.yScale = -1;
    barLabel.fontSize = 25;
    barLabel.fontColor = [UIColor redColor];
    [healthBar addChild:barLabel];
    
    SKShapeNode *redHealth = [SKShapeNode new];
    CGMutablePathRef myPath = CGPathCreateMutable();
    CGPathAddRect(myPath, NULL, CGRectMake(-100, redHealthBarHeight, 200, 20));
    redHealth.xScale = -1;
    redHealth.path = myPath;
    redHealth.fillColor = [UIColor colorWithRed:1.0 green:0.6 blue:1.0 alpha:1.0];
    redHealth.strokeColor = [UIColor clearColor];
    redHealth.name = @"redHealthMeter";
    [self addChild:redHealth];
}

-(void)loadBluePhysicalStrength {
    PhysicalStrength *physicalStrength = [PhysicalStrength physicalStrengthWithFontNamed:GAME_FONT];
    physicalStrength.name = @"bluePhysicalStrngth";
    [self addChild:physicalStrength];
    
    SKShapeNode *strngthBar = [SKShapeNode shapeNodeWithRect:CGRectMake(-100, blueStrengthBarHeight, 200, 20)];
    strngthBar.fillColor = [UIColor clearColor];
    strngthBar.strokeColor = [UIColor yellowColor];
    strngthBar.glowWidth = 3;
    [self addChild:strngthBar];
    
    SKLabelNode *barLabel = [SKLabelNode labelNodeWithFontNamed:GAME_FONT];
    barLabel.text = @"Endurance";
    barLabel.position = CGPointMake(0, blueStrengthBarHeight);
    barLabel.fontSize = 25;
    barLabel.fontColor = [UIColor colorWithRed:0 green:0.56 blue:0 alpha:1.0];
    [strngthBar addChild:barLabel];

    SKShapeNode *blueStrength = [SKShapeNode new];
    CGMutablePathRef myPath = CGPathCreateMutable();
    CGPathAddRect(myPath, NULL, CGRectMake(-100, blueStrengthBarHeight, 200, 20));
    blueStrength.path = myPath;
    blueStrength.fillColor = [UIColor colorWithRed:0.12 green:1.0 blue:0.12 alpha:1.0];
    blueStrength.strokeColor = [UIColor clearColor];
    blueStrength.name = @"blueStrengthMeter";
    [self addChild:blueStrength];
}

-(void)loadRedPhysicalStrength {
    PhysicalStrength *physicalStrength = [PhysicalStrength physicalStrengthWithFontNamed:GAME_FONT];
    physicalStrength.name = @"redPhysicalStrngth";
    [self addChild:physicalStrength];
    
    SKShapeNode *strngthBar = [SKShapeNode shapeNodeWithRect:CGRectMake(-100, redStrengthBarHeight, 200, 20)];
    strngthBar.fillColor = [UIColor clearColor];
    strngthBar.strokeColor = [UIColor yellowColor];
    strngthBar.glowWidth = 3;
    [self addChild:strngthBar];
    
    SKLabelNode *barLabel = [SKLabelNode labelNodeWithFontNamed:GAME_FONT];
    barLabel.text = @"Endurance";
    barLabel.xScale = -1;
    barLabel.yScale = -1;
    barLabel.position = CGPointMake(0, redStrengthBarHeight + 20);
    barLabel.fontSize = 25;
    barLabel.fontColor = [UIColor colorWithRed:0 green:0.56 blue:0 alpha:1.0];
    [strngthBar addChild:barLabel];
    
    SKShapeNode *redStrength = [SKShapeNode new];
    CGMutablePathRef myPath = CGPathCreateMutable();
    CGPathAddRect(myPath, NULL, CGRectMake(-100, redStrengthBarHeight, 200, 20));
    redStrength.path = myPath;
    redStrength.fillColor = [UIColor colorWithRed:0.12 green:1.0 blue:0.12 alpha:1.0];
    redStrength.strokeColor = [UIColor clearColor];
    redStrength.name = @"redStrengthMeter";
    [self addChild:redStrength];
}

-(void)clear {
    [self removeAllChildren];
    if (adcounter == 2) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"requestAd" object:nil];
    }
    MenuScene *scene = [[MenuScene alloc] initWithSize:self.frame.size];
    scene.scaleMode = SKSceneScaleModeAspectFill;
    [self.view presentScene:scene];
}

-(void)gameOver {
    [progress removeFromParent];
    
    self.userInteractionEnabled = NO;
    [self removeAllActions];
    
    PointsLabel *pointsLabelBlue = (PointsLabel *)[self childNodeWithName:@"bluePointsLabel"];
    PointsLabel *pointsLabelRed = (PointsLabel *)[self childNodeWithName:@"redPointsLabel"];
    
    SKLabelNode *winLabel = [SKLabelNode labelNodeWithFontNamed:GAME_FONT];
    winLabel.fontSize = 70;
    winLabel.fontColor = [UIColor yellowColor];
    winLabel.text = @" WINNER!!!";
    winLabel.name = @"winLabel";
    
    if (pointsLabelBlue.number == 0 || pointsLabelRed.number > pointsLabelBlue.number) {
        winLabel.position = CGPointMake(0, 160);
        winLabel.xScale = -1;
        winLabel.yScale = -1;
        [self addChild:winLabel];
    }
    else if (pointsLabelRed.number == 0 || pointsLabelBlue.number > pointsLabelRed.number) {
        winLabel.position = CGPointMake(0, -160);
        [self addChild:winLabel];
    }
    else {
        SKLabelNode *drawLabel = [SKLabelNode labelNodeWithFontNamed:GAME_FONT];
        drawLabel.fontSize = 70;
        drawLabel.zRotation = M_PI / 2;
        drawLabel.fontColor = [UIColor yellowColor];
        drawLabel.position = CGPointMake(155, 0);
        drawLabel.text = @" DRAW ~~~ ";
        [self addChild:drawLabel];
    }
    
    SKLabelNode *tapToResetLabel = [SKLabelNode labelNodeWithFontNamed:GAME_FONT];
    tapToResetLabel.text = @"Tap to Restart";
    tapToResetLabel.fontColor = [UIColor orangeColor];
    tapToResetLabel.fontSize = 30;
    tapToResetLabel.name = @"tapToResetLabel";
    tapToResetLabel.position = CGPointMake(-155, 10);
    tapToResetLabel.zRotation = M_PI / 2;
    
    [self runAction:[SKAction playSoundFileNamed:@"boxingBell.wav" waitForCompletion:NO]];
    [self runAction:[SKAction playSoundFileNamed:@"audienceScream.mp3" waitForCompletion:YES]];
    
    if (adcounter == 2) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"showAd" object:nil];
        self.userInteractionEnabled = YES;
        [self addChild:tapToResetLabel];
        [self animatieWithPulse:tapToResetLabel];
        adcounter = 0;
    }
    else {
        [self runAction:[SKAction waitForDuration:3] completion:^{
            self.userInteractionEnabled = YES;
            [self addChild:tapToResetLabel];
            [self animatieWithPulse:tapToResetLabel ];
        }];
        adcounter++;
    }
    self.isGameOver = YES;
}

// -- While Loop --
-(void)didSimulatePhysics {
    if (!self.isGameOver) {
        [self handleBluePoints];
        [self handleRedPoints];
    }
}

-(void)handleBluePoints {
    PointsLabel *pointsLabel = (PointsLabel *)[self childNodeWithName:@"bluePointsLabel"];
    SKShapeNode *blueHealth = (SKShapeNode *)[self childNodeWithName:@"blueHealthMeter"];
    
    if (!self.isGameOver && redState < 3 && redState > 0 && blueState != 3 && pointsLabel.number > 0) {
        [pointsLabel decrease];
        
        CGMutablePathRef myPath = CGPathCreateMutable();
        float percent = (float)pointsLabel.number/30;
        CGPathAddRect(myPath, NULL, CGRectMake(-100, blueHealthBarHeight, percent*200, 20));
        blueHealth.path = myPath;
        
        [self runAction:[SKAction playSoundFileNamed:@"Punch.wav" waitForCompletion:NO]];
        
        redState = 0;
    }
    else if (!self.isGameOver && redState < 3 && redState > 0 && blueState == 3 && pointsLabel.number > 0) {
        [self runAction:[SKAction playSoundFileNamed:@"whoosh.wav" waitForCompletion:NO]];
        redState = 0;
    }
    
    if (pointsLabel.number <= 10) {
        if (!SoundEffectIsPlayed) {
            SoundEffectIsPlayed = 1;
            [self runAction:[SKAction playSoundFileNamed:@"ClappingCheers.mp3" waitForCompletion:YES] completion:^{
                SoundEffectIsPlayed = 0;
            }];
        }
    }
    
    if (pointsLabel.number == 0) {
        pointsLabel.number = 0;
        [self gameOver];
    }
}

-(void)handleRedPoints {
    PointsLabel *pointsLabel = (PointsLabel *)[self childNodeWithName:@"redPointsLabel"];
    SKShapeNode *redHealth = (SKShapeNode *)[self childNodeWithName:@"redHealthMeter"];
    
    if (!self.isGameOver && blueState < 3 && blueState > 0 && redState != 3 && pointsLabel.number > 0) {
        [pointsLabel decrease];
        
        CGMutablePathRef myPath = CGPathCreateMutable();
        float percent = (float)pointsLabel.number/30;
        CGPathAddRect(myPath, NULL, CGRectMake(-100, redHealthBarHeight, percent*200, 20));
        redHealth.path = myPath;
        
        [self runAction:[SKAction playSoundFileNamed:@"Punch.wav" waitForCompletion:NO]];
        
        blueState = 0;
    }
    else if (!self.isGameOver && blueState < 3 && blueState > 0 && redState == 3 && pointsLabel.number > 0) {
        [self runAction:[SKAction playSoundFileNamed:@"whoosh.wav" waitForCompletion:NO]];
        blueState = 0;
    }
    if (pointsLabel.number <= 10) {
        if (!SoundEffectIsPlayed) {
            SoundEffectIsPlayed = 1;
            
            [self runAction:[SKAction playSoundFileNamed:@"ClappingCheers.mp3" waitForCompletion:YES] completion:^{
                SoundEffectIsPlayed = 0;
            }];
        }
    }
    
    if (pointsLabel.number == 0) {
        pointsLabel.number = 0;
        [self gameOver];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.isGameOver) {
        [self.audioPlayer stop];
        [self clear];
    }
    else if (!self.isStarted) {
        [self start];
    }
    else {
        UITouch *touch = [touches anyObject];
        CGPoint location = [touch locationInNode:self];
        SKNode *node = [self nodeAtPoint:location];
    
        PhysicalStrength *blueStrength = (PhysicalStrength *)[self childNodeWithName:@"bluePhysicalStrngth"];
        PhysicalStrength *redStrength = (PhysicalStrength *)[self childNodeWithName:@"redPhysicalStrngth"];
        SKShapeNode *blueStrengthShape = (SKShapeNode *)[self childNodeWithName:@"blueStrengthMeter"];
        SKShapeNode *redStrengthShape = (SKShapeNode *)[self childNodeWithName:@"redStrengthMeter"];
    
        if ([node.name isEqualToString:@"defenseButtonBlue" ] ) {
            [player1 runAction:[SKAction sequence:@[[SKAction rotateToAngle:0 duration:moveDur],
                                                    [SKAction moveTo:CGPointMake(0, -120) duration:moveDur]]]];
            [player1 blueDefend];
            blueState = 3;
            
            __block int defendTime = 3;
            SKAction *countdDefendTime = [SKAction runBlock:^{
                defendTime--;
            }];
            SKAction *wait = [SKAction waitForDuration:1.0];
            SKAction *countdDefendTimeAndWait = [SKAction sequence:@[countdDefendTime, wait]];
            [self runAction:[SKAction repeatAction:countdDefendTimeAndWait count:3] completion:^{
                player1.position = CGPointMake(0, -100);
                [player1 blueBackOrigin];
                blueState = 0;
            }];

        }
        else if ([node.name isEqualToString:@"punchLeftButtonBlue" ]
                 && blueState == 0
                 && blueStrength.strength > 0) {
            [player1 runAction:[SKAction sequence:@[[SKAction moveTo:CGPointMake(20, -80) duration:moveDur],
                                                [SKAction rotateToAngle:-0.6 duration:moveDur]]]];
            blueState = 1;
        
            [blueStrength decrease];
            CGMutablePathRef myPath = CGPathCreateMutable();
            float percent = (float)blueStrength.strength / sceneEnduration;
            CGPathAddRect(myPath, NULL, CGRectMake(-100, blueStrengthBarHeight, percent*200, 20));
            blueStrengthShape.path = myPath;
        }
        else if ([node.name isEqualToString:@"punchRightButtonBlue" ]
                && blueState == 0
                && blueStrength.strength > 0) {
            [player1 runAction:[SKAction sequence:@[[SKAction moveTo:CGPointMake(20, -80) duration:moveDur],
                                                    [SKAction rotateToAngle:0.6 duration:moveDur]]]];
            blueState = 2;
        
            [blueStrength decrease];
            CGMutablePathRef myPath = CGPathCreateMutable();
            float percent = (float)blueStrength.strength/sceneEnduration;
            CGPathAddRect(myPath, NULL, CGRectMake(-100, blueStrengthBarHeight, percent*200, 20));
            blueStrengthShape.path = myPath;
        }
    
        if ([node.name isEqualToString:@"defenseButtonRed" ] ) {
            [player2 runAction:[SKAction sequence:@[[SKAction rotateToAngle:0 duration:moveDur],
                                                    [SKAction moveTo:CGPointMake(0, 120) duration:moveDur]]]];
            [player2 redDefend];
            redState = 3;
            __block int defendTime = 3;
            SKAction *countdDefendTime = [SKAction runBlock:^{
                defendTime--;
            }];
            SKAction *wait = [SKAction waitForDuration:1.0];
            SKAction *countdDefendTimeAndWait = [SKAction sequence:@[countdDefendTime, wait]];
            [self runAction:[SKAction repeatAction:countdDefendTimeAndWait count:3] completion:^{
                player2.position = CGPointMake(0, 100);
                [player2 redBackOrigin];
                redState = 0;
            }];
        }
        else if ([node.name isEqualToString:@"punchLeftButtonRed" ]
                && redState == 0
                && redStrength.strength > 0) {
            [player2 runAction:[SKAction sequence:@[[SKAction moveTo:CGPointMake(-20, 80) duration:moveDur],
                                                [SKAction rotateToAngle:-0.6 duration:moveDur]]]];
            redState = 1;
        
            [redStrength decrease];
            CGMutablePathRef myPath = CGPathCreateMutable();
            float percent = (float)redStrength.strength/sceneEnduration;
            CGPathAddRect(myPath, NULL, CGRectMake(-100, redStrengthBarHeight, percent*200, 20));
            redStrengthShape.path = myPath;
        }
        else if ([node.name isEqualToString:@"punchRightButtonRed" ]
                && redState == 0
                && redStrength.strength > 0) {
            [player2 runAction:[SKAction sequence:@[[SKAction moveTo:CGPointMake(20, 80) duration:moveDur],
                                                    [SKAction rotateToAngle:0.6 duration:moveDur]]]];
            redState = 2;
            [redStrength decrease];
            CGMutablePathRef myPath = CGPathCreateMutable();
            float percent = (float)redStrength.strength/sceneEnduration;
            CGPathAddRect(myPath, NULL, CGRectMake(-100, redStrengthBarHeight, percent*200, 20));
            redStrengthShape.path = myPath;
        }
    }
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    if ([node.name isEqualToString:@"defenseButtonBlue" ] ||
        [node.name isEqualToString:@"punchLeftButtonBlue" ] ||
        [node.name isEqualToString:@"punchRightButtonBlue" ]) {
        [player1 runAction:[SKAction sequence:@[
                                                [SKAction rotateToAngle:0 duration:moveDur],
                                                [SKAction moveTo:CGPointMake(0, -100) duration:moveDur]]]];

        [player1 blueBackOrigin];
        blueState = 0;
        
        if ([node.name isEqualToString:@"defenseButtonBlue" ]){
            PhysicalStrength *blueStrength = (PhysicalStrength *)[self childNodeWithName:@"bluePhysicalStrngth"];
            SKShapeNode *blueStrengthShape = (SKShapeNode *)[self childNodeWithName:@"blueStrengthMeter"];
            [blueStrength increase];
            CGMutablePathRef myPath = CGPathCreateMutable();
            float percent = (float)blueStrength.strength/sceneEnduration;
            CGPathAddRect(myPath, NULL, CGRectMake(-100, blueStrengthBarHeight, percent*200, 20));
            blueStrengthShape.path = myPath;
        }
    }
    
    if ([node.name isEqualToString:@"defenseButtonRed" ] ||
        [node.name isEqualToString:@"punchLeftButtonRed" ] ||
        [node.name isEqualToString:@"punchRightButtonRed" ]) {
        [player2 runAction:[SKAction sequence:@[
                                                [SKAction rotateToAngle:0 duration:moveDur],
                                                [SKAction moveTo:CGPointMake(0, 100) duration:moveDur]]]];
        [player2 redBackOrigin];
        redState = 0;
        
        if ([node.name isEqualToString:@"defenseButtonRed" ]) {
            PhysicalStrength *redStrength = (PhysicalStrength *)[self childNodeWithName:@"redPhysicalStrngth"];
            SKShapeNode *redStrengthShape = (SKShapeNode *)[self childNodeWithName:@"redStrengthMeter"];
            [redStrength increase];
            CGMutablePathRef myPath = CGPathCreateMutable();
            float percent = (float)redStrength.strength/sceneEnduration;
            CGPathAddRect(myPath, NULL, CGRectMake(-100, redStrengthBarHeight, percent*200, 20));
            redStrengthShape.path = myPath;
        }
    }
}

-(void)loadBlueButton {
    SKShapeNode *defense= [SKShapeNode shapeNodeWithCircleOfRadius:buttonRadius];
    defense.position = CGPointMake(0, blueButtonPosition);
    defense.fillColor = [UIColor blueColor];
    defense.strokeColor = [UIColor blackColor];
    defense.name = @"defenseButtonBlue";
    [self addChild:defense];
    SKLabelNode *defLabel = [SKLabelNode labelNodeWithFontNamed:GAME_FONT];
    defLabel.text = @"Defend";
    defLabel.fontSize = 15;
    defLabel.position = CGPointMake(0, 30);
    defLabel.fontColor = [UIColor whiteColor];
    [defense addChild:defLabel];
    
    SKShapeNode *punchLeft = [SKShapeNode shapeNodeWithCircleOfRadius:buttonRadius];
    punchLeft.position = CGPointMake(-110, blueButtonPosition);
    punchLeft.fillColor = [UIColor blueColor];
    punchLeft.strokeColor = [UIColor blackColor];
    punchLeft.name = @"punchLeftButtonBlue";
    [self addChild:punchLeft];
    SKLabelNode *punchLeftLabel = [SKLabelNode labelNodeWithFontNamed:GAME_FONT];
    punchLeftLabel.text = @"Left Hook";
    punchLeftLabel.fontSize = 15;
    punchLeftLabel.position = CGPointMake(0, 30);
    punchLeftLabel.fontColor = [UIColor whiteColor];
    [punchLeft addChild:punchLeftLabel];
    
    SKShapeNode *punchRight = [SKShapeNode shapeNodeWithCircleOfRadius:buttonRadius];
    punchRight.position = CGPointMake(110, blueButtonPosition);
    punchRight.fillColor = [UIColor blueColor];
    punchRight.strokeColor = [UIColor blackColor];
    punchRight.name = @"punchRightButtonBlue";
    [self addChild:punchRight];
    SKLabelNode *punchRightLabel = [SKLabelNode labelNodeWithFontNamed:GAME_FONT];
    punchRightLabel.text = @"Right Hook";
    punchRightLabel.fontSize = 15;
    punchRightLabel.position = CGPointMake(0, 30);
    punchRightLabel.fontColor = [UIColor whiteColor];
    [punchRight addChild:punchRightLabel];
}

-(void)loadRedButton {
    SKShapeNode *defense = [SKShapeNode shapeNodeWithCircleOfRadius:buttonRadius];
    defense.position = CGPointMake(0, redButtonPosition);
    defense.fillColor = [UIColor redColor];
    defense.strokeColor = [UIColor blackColor];
    defense.name = @"defenseButtonRed";
    [self addChild:defense];
    SKLabelNode *defLabel = [SKLabelNode labelNodeWithFontNamed:GAME_FONT];
    defLabel.text = @"Defend";
    defLabel.fontSize = 15;
    defLabel.position = CGPointMake(0, -30);
    defLabel.fontColor = [UIColor whiteColor];
    defLabel.xScale = -1;
    defLabel.yScale = -1;
    [defense addChild:defLabel];
    
    SKShapeNode *punchLeft = [SKShapeNode shapeNodeWithCircleOfRadius:buttonRadius];
    punchLeft.position = CGPointMake(110, redButtonPosition);
    punchLeft.fillColor = [UIColor redColor];
    punchLeft.strokeColor = [UIColor blackColor];
    punchLeft.name = @"punchLeftButtonRed";
    [self addChild:punchLeft];
    SKLabelNode *punchLeftLabel = [SKLabelNode labelNodeWithFontNamed:GAME_FONT];
    punchLeftLabel.text = @"Left Hook";
    punchLeftLabel.fontSize = 15;
    punchLeftLabel.position = CGPointMake(0, -30);
    punchLeftLabel.fontColor = [UIColor whiteColor];
    punchLeftLabel.xScale = -1;
    punchLeftLabel.yScale = -1;
    [punchLeft addChild:punchLeftLabel];

    SKShapeNode *punchRight = [SKShapeNode shapeNodeWithCircleOfRadius:buttonRadius];
    punchRight.position = CGPointMake(-110, redButtonPosition);
    punchRight.fillColor = [UIColor redColor];
    punchRight.strokeColor = [UIColor blackColor];
    punchRight.name = @"punchRightButtonRed";
    [self addChild:punchRight];
    SKLabelNode *punchRightLabel = [SKLabelNode labelNodeWithFontNamed:GAME_FONT];
    punchRightLabel.text = @"Right Hook";
    punchRightLabel.fontSize = 15;
    punchRightLabel.position = CGPointMake(0, -30);
    punchRightLabel.fontColor = [UIColor whiteColor];
    punchRightLabel.xScale = -1;
    punchRightLabel.yScale = -1;
    [punchRight addChild:punchRightLabel];
}

- (void)animatieWithPulse:(SKNode *)node  {
    SKAction *disappear = [SKAction fadeAlphaTo:0.0 duration:0.8];
    SKAction *appear = [SKAction fadeAlphaTo:1.0 duration:0.8];
    SKAction *pulse = [SKAction sequence:@[disappear, appear]];
    [node runAction:[SKAction repeatActionForever:pulse]];
}

@end
