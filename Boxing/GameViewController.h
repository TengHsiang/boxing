//
//  GameViewController.h
//  Boxing
//

//  Copyright (c) 2016年 虞登翔. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import "MenuScene.h"
#import "GoogleMobileAds.framework/Headers/GADInterstitial.h"
#import "GoogleMobileAds.framework/Headers/GADInterstitialDelegate.h"

@interface GameViewController : UIViewController <GADInterstitialDelegate> 

@property(nonatomic, strong) GADInterstitial *interstitial;

@end
