//
//  PhysicalStrngth.m
//  Boxing
//
//  Created by 虞登翔 on 2016/3/6.
//  Copyright © 2016年 虞登翔. All rights reserved.
//

#import "PhysicalStrength.h"

const int enduration = 10;

@implementation PhysicalStrength

+(id)physicalStrengthWithFontNamed:(NSString *)fontName {
    PhysicalStrength *physicalStrngth = [PhysicalStrength labelNodeWithFontNamed:fontName];
    physicalStrngth.fontColor = [UIColor clearColor];
    physicalStrngth.strength = enduration;
    return physicalStrngth;
}

-(void)decrease {
    if (self.strength > 0) {
        self.strength--;
    }
}

-(void)increase {
    if (self.strength < enduration) {
        self.strength++;
    }
}

@end
