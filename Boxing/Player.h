//
//  Player.h
//  Boxing
//
//  Created by 虞登翔 on 2016/1/28.
//  Copyright © 2016年 虞登翔. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Player : SKSpriteNode

+ (id) player1;
+ (id) player2;

-(void) blueDefend;
-(void) blueBackOrigin;
-(void) redDefend;
-(void) redBackOrigin;
@end
