//
//  PhysicalStrngth.h
//  Boxing
//
//  Created by 虞登翔 on 2016/3/6.
//  Copyright © 2016年 虞登翔. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface PhysicalStrength : SKLabelNode
@property int strength;

+(id)physicalStrengthWithFontNamed:(NSString *)fontName;
-(void)decrease;
-(void)increase;
@end
