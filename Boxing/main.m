//
//  main.m
//  Boxing
//
//  Created by 虞登翔 on 2016/1/27.
//  Copyright © 2016年 虞登翔. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
