//
//  AppDelegate.h
//  Boxing
//
//  Created by 虞登翔 on 2016/1/27.
//  Copyright © 2016年 虞登翔. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

