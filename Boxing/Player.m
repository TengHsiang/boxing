//
//  Player.m
//  Boxing
//
//  Created by 虞登翔 on 2016/1/28.
//  Copyright © 2016年 虞登翔. All rights reserved.
//

#import "Player.h"

const int gloveSize = 30;

@implementation Player

+ (id) player1 {
    Player *player1 = [Player spriteNodeWithColor:[UIColor blackColor] size:CGSizeMake(0, 0)];
    
    SKShapeNode *body = [SKShapeNode shapeNodeWithCircleOfRadius:50];
    body.position = CGPointMake(0, 0);
    body.fillColor = [SKColor colorWithRed:1.0 green:0.79 blue:0.36 alpha:1.0];
    body.strokeColor = [UIColor blueColor];
    [player1 addChild:body];
    
    SKShapeNode *lefteye = [SKShapeNode shapeNodeWithEllipseOfSize:CGSizeMake(10, 15)];
    lefteye.zPosition = 5;
    lefteye.position = CGPointMake(-15,40);
    lefteye.fillColor = [UIColor whiteColor];
    lefteye.strokeColor = [UIColor whiteColor];
    [player1 addChild:lefteye];
    
    SKShapeNode *lefteyeball = [SKShapeNode shapeNodeWithEllipseOfSize:CGSizeMake(5, 5)];
    lefteyeball.zPosition = 6;
    lefteyeball.position = CGPointMake(0,7);
    lefteyeball.fillColor = [UIColor blackColor];
    lefteyeball.strokeColor = [UIColor blackColor];
    [lefteye addChild:lefteyeball];
    
    SKShapeNode *righteye = [SKShapeNode shapeNodeWithEllipseOfSize:CGSizeMake(10, 15)];
    righteye.zPosition = 5;
    righteye.position = CGPointMake(15,40);
    righteye.fillColor = [UIColor whiteColor];
    righteye.strokeColor = [UIColor whiteColor];
    [player1 addChild:righteye];
    
    SKShapeNode *righteyeball = [SKShapeNode shapeNodeWithEllipseOfSize:CGSizeMake(5, 5)];
    righteyeball.zPosition = 6;
    righteyeball.position = CGPointMake(0,7);
    righteyeball.fillColor = [UIColor blackColor];
    righteyeball.strokeColor = [UIColor blackColor];
    [righteye addChild:righteyeball];
    
    SKShapeNode *lefthand = [SKShapeNode shapeNodeWithCircleOfRadius:gloveSize];
    lefthand.position = CGPointMake(-75, 50);
    lefthand.fillColor = [UIColor blueColor];
    lefthand.strokeColor = [UIColor blueColor];
    lefthand.name = @"bluePlayerLeftHand";
    [player1 addChild:lefthand];
    
    SKShapeNode *righthand = [SKShapeNode shapeNodeWithCircleOfRadius:gloveSize];
    righthand.position = CGPointMake(75, 50);
    righthand.fillColor = [UIColor blueColor];
    righthand.strokeColor = [UIColor blueColor];
    righthand.name = @"bluePlayerRightHand";
    [player1 addChild:righthand];
    
    return player1;
}


+ (id) player2 {
    Player *player2 = [Player spriteNodeWithColor:[UIColor blackColor] size:CGSizeMake(0, 0)];
    
    SKShapeNode *body = [SKShapeNode shapeNodeWithCircleOfRadius:50];
    body.position = CGPointMake(0, 0);
    body.fillColor = [SKColor colorWithRed:1.0 green:0.79 blue:0.36 alpha:1.0];
    body.strokeColor = [UIColor redColor];
    [player2 addChild:body];
    
    SKShapeNode *lefteye = [SKShapeNode shapeNodeWithEllipseOfSize:CGSizeMake(10, 15)];
    lefteye.zPosition = 5;
    lefteye.position = CGPointMake(-15,-40);
    lefteye.fillColor = [UIColor whiteColor];
    lefteye.strokeColor = [UIColor whiteColor];
    [player2 addChild:lefteye];
    
    SKShapeNode *lefteyeball = [SKShapeNode shapeNodeWithEllipseOfSize:CGSizeMake(5, 5)];
    lefteyeball.zPosition = 6;
    lefteyeball.position = CGPointMake(0,-7);
    lefteyeball.fillColor = [UIColor blackColor];
    lefteyeball.strokeColor = [UIColor blackColor];
    [lefteye addChild:lefteyeball];
    
    SKShapeNode *righteye = [SKShapeNode shapeNodeWithEllipseOfSize:CGSizeMake(10, 15)];
    righteye.zPosition = 5;
    righteye.position = CGPointMake(15,-40);
    righteye.fillColor = [UIColor whiteColor];
    righteye.strokeColor = [UIColor whiteColor];
    [player2 addChild:righteye];
    
    SKShapeNode *righteyeball = [SKShapeNode shapeNodeWithEllipseOfSize:CGSizeMake(5, 5)];
    righteyeball.zPosition = 6;
    righteyeball.position = CGPointMake(0,-7);
    righteyeball.fillColor = [UIColor blackColor];
    righteyeball.strokeColor = [UIColor blackColor];
    [righteye addChild:righteyeball];
    
    SKShapeNode *lefthand = [SKShapeNode shapeNodeWithCircleOfRadius:gloveSize];
    lefthand.position = CGPointMake(-75, -50);
    lefthand.fillColor = [UIColor redColor];
    lefthand.strokeColor = [UIColor redColor];
    lefthand.name = @"redPlayerLeftHand";
    [player2 addChild:lefthand];
    
    SKShapeNode *righthand = [SKShapeNode shapeNodeWithCircleOfRadius:gloveSize];
    righthand.position = CGPointMake(75, -50);
    righthand.fillColor = [UIColor redColor];
    righthand.strokeColor = [UIColor redColor];
    righthand.name = @"redPlayerRightHand";
    [player2 addChild:righthand];
    
    return player2;
}

-(void) blueDefend {
    SKShapeNode *lefthand = (SKShapeNode*)[self childNodeWithName:@"bluePlayerLeftHand"];
    [lefthand runAction:[SKAction moveTo:CGPointMake(-30, 60) duration:0.01]];
    
    SKShapeNode *righthand = (SKShapeNode*)[self childNodeWithName:@"bluePlayerRightHand"];
    [righthand runAction:[SKAction moveTo:CGPointMake(30, 60) duration:0.01]];
}

-(void) blueBackOrigin {
    SKShapeNode *lefthand = (SKShapeNode*)[self childNodeWithName:@"bluePlayerLeftHand"];
    [lefthand runAction:[SKAction moveTo:CGPointMake(-75, 50) duration:0.01]];
    
    SKShapeNode *righthand = (SKShapeNode*)[self childNodeWithName:@"bluePlayerRightHand"];
    [righthand runAction:[SKAction moveTo:CGPointMake(75, 50) duration:0.01]];
}

-(void) redDefend {
    SKShapeNode *lefthand = (SKShapeNode*)[self childNodeWithName:@"redPlayerLeftHand"];
    [lefthand runAction:[SKAction moveTo:CGPointMake(-30, -60) duration:0.01]];
    
    SKShapeNode *righthand = (SKShapeNode*)[self childNodeWithName:@"redPlayerRightHand"];
    [righthand runAction:[SKAction moveTo:CGPointMake(30, -60) duration:0.01]];
}

-(void) redBackOrigin {
    SKShapeNode *lefthand = (SKShapeNode*)[self childNodeWithName:@"redPlayerLeftHand"];
    [lefthand runAction:[SKAction moveTo:CGPointMake(-75, -50) duration:0.01]];
    
    SKShapeNode *righthand = (SKShapeNode*)[self childNodeWithName:@"redPlayerRightHand"];
    [righthand runAction:[SKAction moveTo:CGPointMake(75, -50) duration:0.01]];
}

@end
