//
//  GameViewController.m
//  Boxing
//
//  Created by 虞登翔 on 2016/1/27.
//  Copyright (c) 2016年 虞登翔. All rights reserved.
//

#import "GameViewController.h"
#import "GameScene.h"

@implementation GameViewController

@synthesize interstitial = interstitial_;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNotification:) name:@"showAd" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNotification:) name:@"requestAd" object:nil];
    
    // Configure the view.
    SKView * skView = (SKView *)self.view;
    skView.showsFPS = NO;
    skView.showsNodeCount = NO;
    /* Sprite Kit applies additional optimizations to improve rendering performance */
    skView.ignoresSiblingOrder = YES;
    
    // Create and configure the scene.
    MenuScene *scene = [MenuScene sceneWithSize:CGSizeMake(skView.frame.size.height*2, skView.frame.size.width*2)];
    scene.scaleMode = SKSceneScaleModeAspectFill;
    
    // Present the scene.
    [skView presentScene:scene];

    self.interstitial = [self loadAdmobInterstitial];
    
}


- (void)handleNotification:(NSNotification *)notification {
    if ([notification.name isEqualToString:@"showAd"]) {
        if ([self.interstitial isReady]) {
            [self.interstitial presentFromRootViewController:self];
        }
        else {
//            NSLog(@"interstitial is not ready");
        }
    }
    else if ([notification.name isEqualToString:@"requestAd"]) {
        self.interstitial = [self loadAdmobInterstitial];
    }
}

-(GADInterstitial *)loadAdmobInterstitial {
    
//    NSLog(@"Request");
    
    GADInterstitial *interstitial = [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-8649202325359735/5287649005"];
    interstitial.delegate = self;
    GADRequest *request = [GADRequest request];
    request.testDevices = @[ kGADSimulatorID, @"e6a5d8d7e8098d551bde0c5b618acb82" ];
    [interstitial loadRequest:request];
    return interstitial;
}

//- (void)interstitialDidReceiveAd:(GADInterstitial *)interstitial {
//    NSLog(@"interstitialDidReceiveAd");
//}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
