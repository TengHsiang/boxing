//
//  SKScene+menuScene.m
//  Boxing
//
//  Created by 虞登翔 on 2016/4/17.
//  Copyright © 2016年 虞登翔. All rights reserved.
//

#import "MenuScene.h"

static NSString *GAME_FONT = @"AmericanTyperwriter-Bold";
const int Radius = 35;

@implementation MenuScene

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        self.anchorPoint = CGPointMake(0.5, 0.5);
        self.backgroundColor = [UIColor blackColor];
        [self buildButton];
    }
    return self;
}

-(void)buildButton {
    SKShapeNode *startButton= [SKShapeNode shapeNodeWithCircleOfRadius:Radius];
    startButton.position = CGPointMake(-110, -70);
    startButton.fillColor = [UIColor colorWithRed:0.976 green:0.756 blue:0.043 alpha:1.0];
    startButton.strokeColor = [UIColor colorWithRed:0.976 green:0.756 blue:0.043 alpha:1.0];
    startButton.name = @"startButton";
    [self addChild:startButton];
    
//    SKLabelNode *defLabel = [SKLabelNode labelNodeWithFontNamed:GAME_FONT];
//    defLabel.text = @"Defend";
//    defLabel.fontSize = 15;
//    defLabel.position = CGPointMake(0, 30);
//    defLabel.fontColor = [UIColor whiteColor];
//    [defense addChild:defLabel];
    
    SKShapeNode *reviewButton= [SKShapeNode shapeNodeWithCircleOfRadius:Radius];
    reviewButton.position = CGPointMake(0, -70);
    reviewButton.fillColor = [UIColor colorWithRed:0 green:0.721 blue:0.941 alpha:1.0];
    reviewButton.strokeColor = [UIColor colorWithRed:0 green:0.721 blue:0.941 alpha:1.0];
    reviewButton.name = @"reviewButton";
    [self addChild:reviewButton];

    SKShapeNode *infoButton= [SKShapeNode shapeNodeWithCircleOfRadius:Radius];
    infoButton.position = CGPointMake(110, -70);
    infoButton.fillColor = [UIColor colorWithRed:0.443 green:0 blue:0.858 alpha:1.0];
    infoButton.strokeColor = [UIColor colorWithRed:0.443 green:0 blue:0.858 alpha:1.0];
    infoButton.name = @"infoButton";
    [self addChild:infoButton];

}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    SKTransition *transition = [SKTransition flipVerticalWithDuration:0.5];
    
    SKScene *gameScene = [[GameScene alloc] initWithSize:self.size];
    gameScene.scaleMode = SKSceneScaleModeAspectFill;
    [self.view presentScene:gameScene transition:transition];
}


@end
