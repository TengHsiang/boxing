//
//  ArenaGenerator.m
//  Boxing
//
//  Created by 虞登翔 on 2016/2/2.
//  Copyright © 2016年 虞登翔. All rights reserved.
//

#import "ArenaGenerator.h"

@implementation ArenaGenerator

+(id) generatorWithArena {
    SKShapeNode *generatorWithArena = [SKShapeNode shapeNodeWithRect:CGRectMake(-150, -175, 300, 350)];
    generatorWithArena.fillColor = [SKColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:1.0];
    
    SKShapeNode *cornerLeftUp = [SKShapeNode shapeNodeWithCircleOfRadius:7];
    cornerLeftUp.position = CGPointMake(-150, 175);
    cornerLeftUp.fillColor = [UIColor whiteColor];
    cornerLeftUp.strokeColor = [UIColor whiteColor];
    [generatorWithArena addChild:cornerLeftUp];
    
    SKShapeNode *cornerLeftDown = [SKShapeNode shapeNodeWithCircleOfRadius:7];
    cornerLeftDown.position = CGPointMake(-150, -175);
    cornerLeftDown.fillColor = [UIColor whiteColor];
    cornerLeftDown.strokeColor = [UIColor whiteColor];
    [generatorWithArena addChild:cornerLeftDown];
    
    SKShapeNode *cornerRightUp = [SKShapeNode shapeNodeWithCircleOfRadius:7];
    cornerRightUp.position = CGPointMake(150, 175);
    cornerRightUp.fillColor = [UIColor whiteColor];
    cornerRightUp.strokeColor = [UIColor whiteColor];
    [generatorWithArena addChild:cornerRightUp];
    
    SKShapeNode *cornerRightDown = [SKShapeNode shapeNodeWithCircleOfRadius:7];
    cornerRightDown.position = CGPointMake(150, -175);
    cornerRightDown.fillColor = [UIColor whiteColor];
    cornerRightDown.strokeColor = [UIColor whiteColor];
    [generatorWithArena addChild:cornerRightDown];
    
    return generatorWithArena;
}

@end
