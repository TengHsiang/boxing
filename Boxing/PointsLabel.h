//
//  PointsLabel.h
//  Boxing
//
//  Created by 虞登翔 on 2016/2/19.
//  Copyright © 2016年 虞登翔. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface PointsLabel : SKLabelNode
@property int number;

+(id)pointsLabelWithFontNamed:(NSString *)fontName;
-(void)decrease;
@end
