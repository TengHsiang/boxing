//
//  PointsLabel.m
//  Boxing
//
//  Created by 虞登翔 on 2016/2/19.
//  Copyright © 2016年 虞登翔. All rights reserved.
//

#import "PointsLabel.h"

@implementation PointsLabel

+(id)pointsLabelWithFontNamed:(NSString *)fontName {
    PointsLabel *pointsLabel = [PointsLabel labelNodeWithFontNamed:fontName];
    pointsLabel.fontColor = [UIColor clearColor];
    pointsLabel.number = 30;
    return pointsLabel;
}

-(void)decrease {
    self.number--;
}



@end
