//
//  ArenaGenerator.h
//  Boxing
//
//  Created by 虞登翔 on 2016/2/2.
//  Copyright © 2016年 虞登翔. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface ArenaGenerator : SKSpriteNode
+(id) generatorWithArena;
@end
